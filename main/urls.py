from django.urls import path, re_path

from main import admin
from . import views

app_name = "main"

urlpatterns = [
    path('', views.index, name="index"),
    path('admin', admin.admin.site.urls, name="admin"),
    re_path(r'[a-zA-Z0-9]', views.shortResult, name="shorten")
]
