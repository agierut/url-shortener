from django.core.validators import RegexValidator
from django.db import models

alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')


class URL(models.Model):
    urlOriginal = models.CharField(unique=True, max_length=4000)
    urlShortened = models.CharField(primary_key=True, max_length=5, validators=[alphanumeric])

    def __str__(self):
        return self.urlShortened + " --> " + self.urlOriginal
