from django.shortcuts import render, redirect
from .models import URL

import random
import string


def index(request):
    if request.method == "POST":
        website = request.POST.get("url")
        current_path = request.get_full_path()
        record_exists = URL.objects.filter(urlOriginal=website).count()

        if record_exists:
            short_url = URL.objects.get(urlOriginal=website).urlShortened
        else:
            short_url = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(5))
            new_record = URL.objects.create(urlOriginal=website, urlShortened=short_url)
            new_record.save()

        return render(request, "main/result.html", {"shortURL": short_url, "currPath": current_path})
    else:
        return render(request, "main/index.html")


def shortResult(request):
    path = request.path[1:]
    record_exists = URL.objects.filter(urlShortened=path).count()

    if record_exists:
        original_url = URL.objects.get(urlShortened=path).urlOriginal
        return redirect(original_url)
    else:
        return render(request, "main/error.html")
